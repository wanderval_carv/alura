import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

class Data{
	int dia;
	int mes;
	int ano;

	void preencheData (int dia, int mes, int ano) {
        String data = dia+"/"+mes+"/"+ano;
        DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
        df.setLenient (false); 
        try {
            df.parse (data);
            this.dia = dia;
            this.mes = mes;
            this.ano = ano;
        } catch (ParseException ex) {}
    }

    String getFormatada (){
    	return this.dia+"/"+this.mes+"/"+this.ano;
    }
}