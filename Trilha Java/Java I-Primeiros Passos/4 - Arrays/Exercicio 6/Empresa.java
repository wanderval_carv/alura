class Empresa{
	String nome;
	String cnpj;
	Funcionario[] funcionarios;

	void imprimeFuncionarios(){
		for(Funcionario x:this.funcionarios){
			
			if(x == null) continue;
			System.out.println(x.nome);
		}
	}

	void adiciona(Funcionario f) {
		for(int i=0; i < this.funcionarios.length; i++){
			if(this.funcionarios[i] == null){
				this.funcionarios[i] = f;
				break;
			}
		}
    }

    void mostraEmpregados() {
        for (int i = 0; i < this.funcionarios.length; i++) {
        	if(this.funcionarios[i] == null)continue;
            System.out.println("Funcionário na posição: " + i);
            System.out.println("Nome: " + this.funcionarios[i].nome);
            System.out.println("Salário: " + this.funcionarios[i].salario);
        }
    }
    void mostraTodasAsInformacoes(){
    	for (int i = 0; i < this.funcionarios.length; i++) {
        	if(this.funcionarios[i] == null)continue;
            this.funcionarios[i].mostra();
        }
    }
    boolean contem(Funcionario f) {
        for (int i = 0; i < this.funcionarios.length; i++) {
            if (f == this.funcionarios[i]) {
                return true;
            }
        }
        return false;
    }
}