class Init{
	public static void main(String[] args){
        
        Empresa empresa = new Empresa();
        empresa.funcionarios = new Funcionario[10];

        

        Funcionario f1 = new Funcionario();
        f1.nome = "wanderval";
        f1.salario = 150;
        f1.departamento = "T.I.";
        f1.rg = "10001";

        f1.dataEntrada = new Data();        
        f1.dataEntrada.preencheData(30,3,1984);
        empresa.adiciona(f1);

        Funcionario f2 = new Funcionario();
        f2.nome = "Jasmin";
        f2.salario = 150;
        f2.departamento = "T.I.";
        f2.rg = "10002";

        f2.dataEntrada = new Data();        
        f2.dataEntrada.preencheData(30,3,1984);
        empresa.adiciona(f2);

        //empresa.mostraEmpregados();
        empresa.mostraTodasAsInformacoes();
 
	}
}
