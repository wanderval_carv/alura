class Empresa{
	private String nome;
	private String cnpj;
	private Funcionario[] funcionarios;

	void imprimeFuncionarios(){
		for(Funcionario x:this.funcionarios){
			
			if(x == null) continue;
			System.out.println(x.getNome());
		}
	}

	void adiciona(Funcionario f) {
		for(int i=0; i < this.funcionarios.length; i++){
			if(this.funcionarios[i] == null){
				this.funcionarios[i] = f;
				break;
			}
		}
    }

    void mostraEmpregados() {
        for (int i = 0; i < this.funcionarios.length; i++) {
        	if(this.funcionarios[i] == null)continue;
            System.out.println("Funcionário na posição: " + i);
            System.out.println("Nome: " + this.funcionarios[i].getNome());
            System.out.println("Salário: " + this.funcionarios[i].getSalario());
        }
    }
    void mostraTodasAsInformacoes(){
    	for (int i = 0; i < this.funcionarios.length; i++) {
        	if(this.funcionarios[i] == null)continue;
            this.funcionarios[i].mostra();
        }
    }
    boolean contem(Funcionario f) {
        for (int i = 0; i < this.funcionarios.length; i++) {
            if (f == this.funcionarios[i]) {
                return true;
            }
        }
        return false;
    }

    //gettter and setter
    public void setNome(String nome){this.nome = nome;}
    public String getNome(){return this.nome;}

    public void setCnpj(String cnpj){this.cnpj = cnpj;}
    public String getCnpj(){return this.cnpj;}

    public void setFuncionario(Funcionario[] funcionario){this.funcionarios = funcionario;}
    public Funcionario getFuncionario(int posicao){return this.funcionarios[posicao];}
}