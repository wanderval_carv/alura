class Init{
	public static void main(String[] args){
        
        Empresa empresa = new Empresa(10);
        //empresa.funcionarios = new Funcionario[10];
        //empresa.setFuncionario(new Funcionario[10]);
        

        Funcionario f1 = new Funcionario();
        f1.setNome("wanderval");
        f1.setSalario(150);
        f1.setDepartamento("T.I.");
        f1.setRg("10001");

        Data data = new Data();
        data.preencheData(5,5,2016);
        f1.setDataEntrada(data);

        empresa.adiciona(f1);

        f1.mostra();
 
	}
}
