class TestarFuncionario{
	public static void main(String[] args){
		Funcionario f1 = new Funcionario();
		f1.nome = "Hugo";
		f1.salario = 100;
		f1.recebeAumento(50);

		System.out.println("salario atual:" + f1.salario);
        System.out.println("ganho anual:" + f1.calculaGanhoAnual());
	}
}

class Funcionario{
	String nome;
	String departamento;
	double salario;
	String dataEntrada;
	String rg;

	void recebeAumento(double aumentoSalario){
		this.salario += aumentoSalario; 
	}
	double calculaGanhoAnual(){
		return this.salario * 12;
	}
}