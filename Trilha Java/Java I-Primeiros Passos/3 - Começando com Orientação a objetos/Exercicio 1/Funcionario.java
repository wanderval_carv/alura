class Funcionario{
	String nome;
	String departamento;
	double salario;
	String dataEntrada;
	String rg;

	void recebeAumento(double aumentoSalario){
		this.salario += aumentoSalario; 
	}
	double calculaGanhoAnual(){
		return this.salario * 12;
	}
}