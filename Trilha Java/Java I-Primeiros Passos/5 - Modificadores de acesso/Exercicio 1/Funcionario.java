class Funcionario{
	private String nome;
	private String departamento;
	private double salario;
	private Data dataEntrada;
	private String rg;

	public void recebeAumento(double aumentoSalario){
		this.salario += aumentoSalario; 
	}
	public double calculaGanhoAnual(){
		return this.salario * 12;
	}
	public void mostra() {
        System.out.println("Nome: " + this.nome);
        System.out.println("Departamento: " + this.departamento);
        System.out.println("Salário: " + this.salario);
        System.out.println("RG: " + this.rg);
        System.out.println("GanhoAnual: " + calculaGanhoAnual());
        System.out.println("Data de entrada: " + this.dataEntrada.getFormatada());
    }

    //getter and setter
    public void setNome(String nome){this.nome = nome;}
    public String getNome(){return this.nome;}

    public void setDepartamento(String departamento){this.departamento = departamento;}
    public String getDepartamento(){return this.departamento;}

    public void setDataEntrada(Data dataEntrada){this.dataEntrada = dataEntrada;}
    public Data getDataEntrada(){return this.dataEntrada;}

    public void setSalario(double salario){this.salario = salario;}
    public double getSalario(){return this.salario;}

    public void setRg(String rg){this.rg = rg;}
    public String getRg(){return this.rg;}
}