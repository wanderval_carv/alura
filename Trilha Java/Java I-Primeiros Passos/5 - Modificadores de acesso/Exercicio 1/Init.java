class Init{
	public static void main(String[] args){
        
        Empresa empresa = new Empresa();
        empresa.funcionarios = new Funcionario[10];

        

        Funcionario f1 = new Funcionario();
        f1.setNome("wanderval");
        f1.setSalario(150);
        f1.setDepartamento("T.I.");
        f1.setRg("10001");

        /*f1.dataEntrada = new Data();        
        f1.dataEntrada.preencheData(30,3,1984);*/

        Data data = new Data();
        data.preencheData(5,5,2016);
        f1.setDataEntrada(data);

        empresa.adiciona(f1);

        Funcionario f2 = new Funcionario();
        f2.setNome("Jasmin");
        f2.setSalario(150);
        f2.setDepartamento("T.I.");
        f2.setRg("10002");

        Data data2 = new Data();
        data2.preencheData(5,5,2016);
        f2.setDataEntrada(data2);
        
        empresa.adiciona(f2);

        //empresa.mostraEmpregados();
        if(empresa.contem(f1)){
                System.out.println("contem!");
        }else{
                System.out.println("não contem!");
        }
 
	}
}
