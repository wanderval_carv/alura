class Init{
	public static void main(String[] args){
        
        Empresa empresa = new Empresa();
        empresa.funcionarios = new Funcionario[10];

        

        Funcionario f1 = new Funcionario();
        f1.setNome("wanderval");
        f1.setSalario(150);
        f1.setDepartamento("T.I.");
        f1.setRg("10001");

        Data data = new Data();
        data.preencheData(5,5,2016);
        f1.setDataEntrada(data);

        empresa.adiciona(f1);

        //empresa.mostraEmpregados();
        if(empresa.contem(f1)){
                System.out.println("contem!");
        }else{
                System.out.println("não contem!");
        }
 
	}
}
