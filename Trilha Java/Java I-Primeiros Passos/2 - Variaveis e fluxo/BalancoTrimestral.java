class BalancoTrimestral{
	public static void main(String[] args) {
    	//exercicio 1
        int gastosJaneiro   = 15000;
        int gastosFevereiro = 23000;
        int gastosMarco     = 17000;

        int gastosTrimestre = gastosJaneiro + gastosFevereiro + gastosMarco;  
        
        System.out.println("Gasto trimestral R$:"+gastosTrimestre);

        //exercico 2
        int mediaMensal     = gastosTrimestre/3; 

        System.out.println("Valor da média mensal:"+mediaMensal);
    }
}

