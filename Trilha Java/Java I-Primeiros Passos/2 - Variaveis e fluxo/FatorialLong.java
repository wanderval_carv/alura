//exercicio 7
public class FatorialLong {

    public static void main(String[] args) {
   
        long aux = 1;
        int numero = 1;
        
        //forma 1
        while (numero < 30) {
            aux  += (aux * numero);
            numero++;
        }
         System.out.println("valor:"+aux);


        aux = 1;
        //forma 2 
        for(int i = 1; i < 30; i++){
        	aux  += (aux * i);
        }
        System.out.println("valor:"+aux);
        
    }
}