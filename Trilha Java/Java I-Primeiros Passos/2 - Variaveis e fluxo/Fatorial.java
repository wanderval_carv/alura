//exercicio 6
public class Fatorial {

    public static void main(String[] args) {
   
        int aux = 1;
        int numero = 1;
        
        //forma 1
        while (numero < 10) {
            aux  += (aux * numero);
            numero++;
        }
         System.out.println("valor:"+aux);


        aux = 1;
        //forma 2 
        for(int i = 1; i < 10; i++){
        	aux  += (aux * i);
        }
        System.out.println("valor:"+aux);
        
    }
}