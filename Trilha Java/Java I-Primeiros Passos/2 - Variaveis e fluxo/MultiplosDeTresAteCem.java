//exercicio 5
public class MultiplosDeTresAteCem {

    public static void main(String[] args) {
        int numero = 1;
   
        //forma 1
        while (numero < 100) {
        	if(numero % 3 == 0){
        		System.out.println(numero);
        	}
        	numero++;
        }

        //forma 2 
        for(int i = 0; i < 100; i=i+3){
        	System.out.println(i);
        }
        
    }
}