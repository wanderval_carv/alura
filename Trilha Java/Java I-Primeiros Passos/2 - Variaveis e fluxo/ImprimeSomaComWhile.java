//exercicio 4
public class ImprimeSomaComWhile {

    public static void main(String[] args) {
        int i = 1;
        int aux = 0;
        while (i < 1000) {
        	aux = aux + i;
            i++;
        }
        System.out.println(aux);
    }
}